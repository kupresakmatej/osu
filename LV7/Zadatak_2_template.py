import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans
import sys
import os

for i in range(1, 7):
    # ucitaj sliku
    img = Image.imread(os.path.join(sys.path[0], f'imgs\\test_{i}.jpg'))

    # prikazi originalnu sliku
    plt.figure()
    plt.title("Originalna slika")
    plt.imshow(img)
    plt.tight_layout()
    plt.show()

    # pretvori vrijednosti elemenata slike u raspon 0 do 1
    img = img.astype(np.float64) / 255

    # transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
    w, h, d = img.shape
    img_array = np.reshape(img, (w*h, d))

    # rezultatna slika
    img_array_aprox = img_array.copy()

    # broj boja
    print(np.unique(img_array_aprox, axis=0).shape[0])

    def recreate_image(codebook, labels, w, h):
        return codebook[labels].reshape(w, h, -1)

    km = KMeans(n_clusters=4, init='k-means++', n_init=5)
    km.fit(img_array_aprox)
    labels = km.predict(img_array_aprox)
    img_array_aprox = recreate_image(km.cluster_centers_, labels, w, h)
    plt.figure()
    plt.title("NeOriginalna slika")
    plt.imshow(img_array_aprox)
    plt.tight_layout()
    plt.show()

    num_clusters = range(1, 5)

    inertia_vals = []
    elbow_vals = []

    # Izračunavanje inerce za svaki broj grupa i spremanje u listu
    for i in num_clusters:
        kmeans = KMeans(n_clusters=i, random_state=42)
        kmeans.fit(img_array)
        inertia_vals.append(kmeans.inertia_)

    plt.plot(num_clusters, inertia_vals, marker='o')
    plt.xlabel('Broj grupa')
    plt.ylabel('Inerca')
    plt.title('Ovisnost inerce o broju grupa')
    plt.show()
