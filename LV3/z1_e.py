import pandas as pd

data = pd.read_csv(
    'C:\\Users\\student\\Desktop\\lv3\\osu\\LV3\\data_C02_emission.csv')

even_cylinders = data[data['Cylinders'] % 2 == 0]

for i in even_cylinders['Cylinders'].unique():
    print(data[data['Cylinders'] == i]['CO2 Emissions (g/km)'].mean().round(2))