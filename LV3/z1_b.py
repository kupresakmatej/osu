import pandas as pd

data = pd.read_csv(
    'C:\\Users\\student\\Desktop\\lv3\\osu\\LV3\\data_C02_emission.csv')

data.sort_values(by=['Fuel Consumption City (L/100km)'],
                 ascending=True, inplace=True)

print(data.head(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']])
print(data.tail(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']])
