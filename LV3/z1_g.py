import pandas as pd

data = pd.read_csv(
    'C:\\Users\\student\\Desktop\\lv3\\osu\\LV3\\data_C02_emission.csv')

result = data[(data['Fuel Type'] == 'D') & (data['Cylinders'] == 4)].sort_values(
    by=['Fuel Consumption City (L/100km)']).head(1)

print(result)
