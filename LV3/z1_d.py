import pandas as pd

data = pd.read_csv(
    'C:\\Users\\student\\Desktop\\lv3\\osu\\LV3\\data_C02_emission.csv')

audi = data[data['Make'] == 'Audi']

print(audi.count())

audi_four_cylinders = audi[data['Cylinders'] == 4]

print(audi_four_cylinders)

co2_audi = audi_four_cylinders['CO2 Emissions (g/km)']

co2_avg = co2_audi.mean()

print(co2_avg)
