#import bibilioteka
from sklearn import datasets
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches


#učitavanje dataseta
iris = datasets.load_iris()

df = pd.DataFrame(data=np.c_[iris['data'], iris['target']],
                  columns= iris['feature_names'] + ['target']).astype({'target': int}).assign(species=lambda x: x['target'].map(dict(enumerate(iris['target_names']))))

sepal_length_versi = df[df['species'] == 'versicolor']['sepal length (cm)']
petal_length_versi = df[df['species'] == 'versicolor']['petal length (cm)']

sepal_length_virg = df[df['species'] == 'virginica']['sepal length (cm)']
petal_length_virg = df[df['species'] == 'virginica']['petal length (cm)']

classes = ['Versicolor','Virginica']
class_colours =['b', 'r']
legend = []

for i in range(0, len(class_colours)):
    legend.append(mpatches.Rectangle((0,0),1,1,fc=class_colours[i]))

plt.scatter(sepal_length_versi, petal_length_versi, c='b', marker='.', linewidths=1.5)
plt.scatter(sepal_length_virg, petal_length_virg, c='r', marker='.', linewidths=1.5)
plt.title('sepal and petal lengths')
plt.legend(legend, classes, loc=4)
plt.xlabel('sepal length')
plt.ylabel('petal length')
plt.show()

petal_width_versi_mean = df[df['species'] == 'versicolor']['sepal width (cm)'].mean()
petal_width_virg_mean = df[df['species'] == 'virginica']['sepal width (cm)'].mean()
petal_width_set_mean = df[df['species'] == 'setosa']['sepal width (cm)'].mean()

x = ['Versicolor', 'Virginica', 'Setosa']
y = [petal_width_versi_mean, petal_width_virg_mean, petal_width_set_mean]

plt.bar(x, y)
plt.xlabel('types of flower')
plt.ylabel('mean')
plt.title('mean for types of flowers')
plt.show()

petal_width_virg = df[df['species'] == 'virginica']['petal width (cm)']

result = len(df[df['species'] == 'virginica']['petal width (cm)'] > petal_width_virg_mean)

print(result)