#import bibilioteka
from sklearn import datasets
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout


#učitavanje dataseta
iris = datasets.load_iris()


##################################################
#1. zadatak
##################################################


#a)
df = pd.DataFrame(data=np.c_[iris['data'], iris['target']],
                  columns= iris['feature_names'] + ['target']).astype({'target': int}).assign(species=lambda x: x['target'].map(dict(enumerate(iris['target_names']))))

sepal_length_versi = df[df['species'] == 'versicolor']['sepal length (cm)']
petal_length_versi = df[df['species'] == 'versicolor']['petal length (cm)']

sepal_length_virg = df[df['species'] == 'virginica']['sepal length (cm)']
petal_length_virg = df[df['species'] == 'virginica']['petal length (cm)']

classes = ['Versicolor','Virginica']
class_colours =['b', 'r']
legend = []

for i in range(0, len(class_colours)):
    legend.append(mpatches.Rectangle((0,0),1,1,fc=class_colours[i]))

plt.scatter(sepal_length_versi, petal_length_versi, c='b', marker='.', linewidths=1.5)
plt.scatter(sepal_length_virg, petal_length_virg, c='r', marker='.', linewidths=1.5)
plt.title('sepal and petal lengths')
plt.legend(legend, classes, loc=4)
plt.xlabel('sepal length')
plt.ylabel('petal length')
plt.show()

#b)
petal_width_versi_mean = df[df['species'] == 'versicolor']['sepal width (cm)'].mean()
petal_width_virg_mean = df[df['species'] == 'virginica']['sepal width (cm)'].mean()
petal_width_set_mean = df[df['species'] == 'setosa']['sepal width (cm)'].mean()

x = ['Versicolor', 'Virginica', 'Setosa']
y = [petal_width_versi_mean, petal_width_virg_mean, petal_width_set_mean]

plt.bar(x, y)
plt.xlabel('types of flower')
plt.ylabel('mean')
plt.title('mean for types of flowers')
plt.show()

#c)
petal_width_virg = df[df['species'] == 'virginica']['petal width (cm)']

result = len(df[df['species'] == 'virginica']['petal width (cm)'] > petal_width_virg_mean)

print(result)


##################################################
#2. zadatak
##################################################


#a)
x = df.iloc[:, [0, 1, 2, 3]].values

wcss = []

for i in range(1, 11):
    kmeans = KMeans(n_clusters = i, init = 'k-means++', 
                    max_iter = 300, n_init = 10, random_state = 0)
    kmeans.fit(x)
    wcss.append(kmeans.inertia_)

#b)

plt.plot(range(1, 11), wcss)
plt.title('The elbow method')
plt.xlabel('Number of clusters')
plt.ylabel('WCSS')
plt.show()

#c)

kmeans = KMeans(n_clusters = 3, init = 'k-means++',n_init = 5, random_state = 0)

y_kmeans = kmeans.fit_predict(x)

#d)

plt.figure()
plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:,1], 
            s = 100, c = 'red', label = 'Centroids')
plt.scatter(x[y_kmeans == 0, 0], x[y_kmeans == 0, 1], s = 100, c = 'blue', label = 'Setosa')
plt.scatter(x[y_kmeans == 1, 0], x[y_kmeans == 1, 1], s = 100, c = 'yellow', label = 'Versicolor')
plt.scatter(x[y_kmeans == 2, 0], x[y_kmeans == 2, 1], s = 100, c = 'green', label = 'Virginica')
plt.legend()
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('kmeans clusters with centroids')
plt.show()

#e)



##################################################
#3. zadatak
##################################################

df_np = df.to_numpy()

X_data = df_np[:,0:4]
Y_data=df_np[:,4]

print("\nVrijednosti prije skaliranja")
print(X_data[:5,:])
print("Izlazne vrijednosti")
print(Y_data[:5])

scaler = StandardScaler().fit(X_data)

X_data = scaler.transform(X_data)

Y_data = tf.keras.utils.to_categorical(Y_data,3)

print("\nVrijednosti prije skaliranja")
print(X_data[:5,:])
print("Izlazne vrijednosti")
print(Y_data[:5,:])

#Split training and test data
X_train,X_test,Y_train,Y_test = train_test_split( X_data, Y_data, test_size=0.25)

print("\nDimenzije trening testa")
print(X_train.shape, Y_train.shape, X_test.shape, Y_test.shape)

#a)

model = keras.Sequential()

model.add(Dense(10, activation="relu"))
model.add(Dropout(0.3))
model.add(Dense(7, activation="relu"))
model.add(Dropout(0.3))
model.add(Dense(5, activation="relu"))
model.add(Dropout(0.3))
model.add(Dense(3, activation="softmax"))

model.build(input_shape=X_train.shape)

model.summary()

#b)

#c)

#d)

#e)

#f)