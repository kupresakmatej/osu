from sklearn import datasets
from sklearn . model_selection import train_test_split
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv(
    'C:\\Users\\student\\Desktop\\lv4\\osu\\LV4\\data_C02_emission.csv')

data.drop(["Make", "Model"], axis = 1)

input_variables = ['Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)', 'Engine Size (L)', 'Cylinders']

output_variable = ['CO2 Emissions (g/km)']

x = data[input_variables].to_numpy()
y = data[output_variable].to_numpy()

x_train , x_test , y_train , y_test = train_test_split(x , y , test_size = 0.2, random_state = 1)

plt.scatter(x_train[:, 0], y_train, color="blue", label="trening")
plt.scatter(x_test[:, 0], y_test, color="red", label="test")
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend()
plt.show()

plt.scatter(x_train[:,1], y_train, color="blue")
plt.scatter(x_test[:,1], y_test, color="red")
plt.xlabel('Fuel Consumption Hwy (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

plt.scatter(x_train[:,2], y_train, color="blue")
plt.scatter(x_test[:,2], y_test, color="red")
plt.xlabel('Fuel Consumption Comb (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

plt.scatter(x_train[:,3], y_train, color="blue")
plt.scatter(x_test[:,3], y_test, color="red")
plt.xlabel('Fuel Consumption Comb (mpg)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

plt.scatter(x_train[:,4], y_train, color="blue")
plt.scatter(x_test[:,4], y_test, color="red")
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

plt.scatter(x_train[:,5], y_train, color="blue")
plt.scatter(x_test[:,5], y_test, color="red")
plt.xlabel('Cylinders')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()