while True:
    value = input('Enter the grade between 0.0 and 1.0:')
    try:
       value = float(value)
    except ValueError:
       print('Enter a valid number please.')
       continue
    if 0.0 <= value <= 1.0:
       break
    else:
       print('Enter in the valid range, please: 0.0-1.0')

if(value >= 0.9):
   print('A')
elif(value >= 0.8):
   print('B')
elif(value >= 0.7):
   print('C')
elif(value >= 0.6):
   print('D')
else:
   print('F')