numbers = []

while True:
    user_input = input("User input: ")

    if user_input == "Done":
        break
    
    try:
        val = int(user_input)
    except ValueError:
        print("Not an int!")
        continue

    numbers.append(user_input)

    
numbers.sort()

add_up_value = 0

for number in numbers:
    add_up_value += int(number)
    print(number)

print("User entered", len(numbers), "numbers.")
print("Average value is", add_up_value/len(numbers), ".")
print("Max value is", max(numbers), ".")
print("Min value is", min(numbers), ".")