import numpy as np
import matplotlib.pyplot as plt

# 1. zadatak

x = np.array([1, 3, 3, 2, 1])
y = np.array([1, 1, 2, 2, 1])
plt.plot(x, y, 'b', linewidth=1.5, marker=".", markersize=15)
plt.axis([0, 4, 0, 4])
plt.xlabel('x os')
plt.ylabel('y os')
plt.title('Primjer')
plt.show()

# 2. zadatak

data = np.loadtxt('data.csv', delimiter=',', skiprows=1)

print(f"Veličina polja: {data.shape[0]}")

visina = data[:, 1]
tezina = data[:, 2]

plt.scatter(visina, tezina, c='b', s=1, marker=".", linewidths=1.5)
plt.xlabel("visina")
plt.ylabel("tezina")
plt.show()

visina_50 = data[::50, 1]
tezina_50 = data[::50, 2]

plt.scatter(visina_50, tezina_50, c='b', s=1, marker=".", linewidths=1.5)
plt.xlabel("visina")
plt.ylabel("tezina")
plt.show()

print(f"{max(visina)} max, {visina.mean()} prosjecna")

visina_m = data[data[:, 0] == 1]
visina_z = data[data[:, 0] == 0]

height_min_male = np.min(visina_m[:, 1])
height_max_male = np.max(visina_m[:, 1])
height_mean_male = np.mean(visina_m[:, 1])


height_min_female = np.min(visina_z[:, 1])
height_max_female = np.max(visina_z[:, 1])
height_mean_female = np.mean(visina_z[:, 1])


print("Muškarci")
print(height_min_male, height_max_male, height_mean_male)

print("Žene")
print(height_min_female, height_max_female, height_mean_female)

# 3. zadatak

img = plt.imread('road.jpg')
plt.imshow(img, alpha=0.5, cmap="gray")
plt.show()

height, width, _ = img.shape
quarter_width = width // 4
crop_box = (quarter_width, 0, quarter_width * 2, height)
cropped_img = img[:, crop_box[0]:crop_box[2], :]
plt.imshow(cropped_img)
plt.show()

rotated_img = np.rot90(img, k=-1)
plt.imshow(rotated_img)
plt.show()

flipped_img = np.fliplr(img)
plt.imshow(flipped_img)
plt.show()

# 4. zadatak

crna = np.zeros((50, 50))
bijela = np.ones((50, 50)) * 255

bottom_row = np.hstack((bijela, crna))
top_row = np.hstack((crna, bijela))
board = np.vstack((top_row, bottom_row))

plt.imshow(board, cmap='gray')
plt.show()
