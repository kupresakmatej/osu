import matplotlib.pyplot as plt
import numpy as np

data = np.genfromtxt('C:\\Users\\student\\Desktop\\lv2\\osu\\LV2\\data.csv', delimiter=',', skip_header=1)

plt.scatter(data[::50, 2], data[::50, 1])
plt.xlabel("Visina (cm)")
plt.ylabel("Masa (kg)")
plt.title("Odnos visine i mase za svaku pedesetu osobu")
plt.show()
