import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("C:\\Users\\student\\Desktop\\lv2\\osu\\LV2\\road.jpg")
plt.imshow(img, alpha=0.5, cmap ="gray")
plt.show()

height, width, _ = img.shape
quarter_width = width // 4
crop_box = (quarter_width, 0, quarter_width * 2, height)
cropped_img = img[:, crop_box[0]:crop_box[2], :]
plt.imshow(cropped_img)
plt.show()

rotated_img = np.rot90(img, k=-1)
plt.imshow(rotated_img)
plt.show()

flipped_img = np.fliplr(img)
plt.imshow(flipped_img)
plt.show()