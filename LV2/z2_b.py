import matplotlib.pyplot as plt
import numpy as np

# Učitavanje podataka
data = np.genfromtxt('C:\\Users\\student\\Desktop\\lv2\\osu\\LV2\\data.csv', delimiter=',', skip_header=1)

plt.scatter(data[:, 1], data[:, 2])
plt.xlabel("Visina (cm)")
plt.ylabel("Masa (kg)")
plt.title("Odnos visine i mase za sve osobe")
plt.show()
