import numpy as np
import matplotlib.pyplot as plt

# Kreiranje crnog kvadrata dimenzija 50x50
black_square = np.zeros((50, 50))

# Kreiranje bijelog kvadrata dimenzija 50x50
white_square = np.ones((50, 50)) * 255

# Spajanje kvadrata u jedinstvenu sliku
bottom_row = np.hstack((white_square, black_square))
top_row = np.hstack((black_square, white_square))
checkerboard = np.vstack((top_row, bottom_row))

# Prikazivanje slike
plt.imshow(checkerboard, cmap='gray', vmin=0, vmax=255)
plt.show()