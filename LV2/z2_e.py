import matplotlib.pyplot as plt
import numpy as np

data = np.genfromtxt('C:\\Users\\student\\Desktop\\lv2\\osu\\LV2\\data.csv', delimiter=',', skip_header=1)


male_data = data[data[:, 0] == 1]
female_data = data[data[:, 0] == 0]



height_min_male = np.min(male_data[:, 1])
height_max_male = np.max(male_data[:, 1])
height_mean_male = np.mean(male_data[:, 1])


height_min_female = np.min(female_data[:, 1])
height_max_female = np.max(female_data[:, 1])
height_mean_female = np.mean(female_data[:, 1])


print("Muškarci")
print(height_min_male, height_max_male, height_mean_male)

print("Žene")
print(height_min_female, height_max_female, height_mean_female)